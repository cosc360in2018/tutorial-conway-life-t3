"use strict";

function render() {

    let xmlns = "http://www.w3.org/2000/svg"

    let cellSize = 20

    let gameDiv = document.getElementById("game")
    gameDiv.innerHTML = ""

    for (let y = 0; y < gameOfLife.getH(); y++) {
        let row = document.createElementNS(xmlns, "g")
        row.setAttribute("class", "gamerow")
        gameDiv.appendChild(row)

        for (let x = 0; x < gameOfLife.getW(); x++) {
            
            let t = document.createElementNS(xmlns, "rect")
            t.setAttribute("x", x * cellSize)
            t.setAttribute("y", y * cellSize)
            t.setAttribute("width", cellSize)
            t.setAttribute("height", cellSize)
            t.classList.add("cell")

            if (gameOfLife.isAlive(x, y)) {
                t.classList.add("alive")
            }

            let handler = function(evt) {
                gameOfLife.toggleCell(x, y)
                render()
            }
                
            t.addEventListener("mousedown", handler)

            row.appendChild(t)
        }
    }

}